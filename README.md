# Preverjanje stevila tock OS in APS2 (svn oddaje)

##Primer
![Primer](media/primer.jpg)

Pomen vrstic:

* ***Testi z 0 tockami:*** v vrsticah izpiše vsa imena testov, ki so imeli v zadnji vrstici *"Tocke: 0"*
* ***Rezultat #*** - pove koliko testnih primerov je imelo v zadnji vrstici rezultat *"Tocke: 1"*
* ***TRENUTNO TOCK*** - pove koliko točk ste dosegli v trenutnem najnovejšem izvodu datoteke *oddaja.log*

**PROGRAM BO ZMERAJ POKAZAL REZULTAT ZA TRENUTNO NAJVEČJO MAPO r#, KI JO BO NAŠEL**

**!!! NI NUJNO DA PROGRAM PRAVILNO DELUJE. SAMI STE ODGOVORNI ZA SVOJE ODDAJE NALOG IN PREGLEDE TESTOV. AVTOR NE PREVZEMA NIKAKRŠNE ODGOVORNOSTI ZA NASTALE NEVŠEČNOSTI !!!**

##Zagon
Najprej osvežite svn direktorij, da se prenesejo najnovejši primeri ***r#***. To naredite z uporabo `svn update` v mapi ***Naloga#***.
Obstajata dve možnosti zagona testerja:

1. Zagon z dateko ***tester.exe***

    1. Tester prenesemo v mapo, kjer so podmape *Naloga1*, *Naloga2*, *Naloga3*, ... in ga zaženemo s pomočjo argumenta - imena mape, ki jo želimo pregledat v **CMD**, primer:
    `$ tester.exe Naloga1`
    2. Tester prenesemo v posamezno mapo *Naloga#* in ga zaženemo direktno z dvoklikom na *tester.exe* ali pa v CMD z ukazom `$ tester.exe`

2. Zagon z dateko ***tester.py***
    Zagon sprožimo enako kor pri zgornjih primerih *i* in *ii*, le da namesto .exe uporabljamo .py (program uporablja Python 2.7)

##tester.exe
*tester.exe* je ustvarjen s pomočjo prevoda iz python programa v .exe datoteko s programom **pyinstaller**:

    pyinstaller -F tester.py
* Zastavica `-F` zapakira vse potrebno v eno datoteko

Binarna koda v datoteki `tester.exe` v mapi `dist/` je torej prevedena (compiled) koda python programa in **deluje na operacijskih sistemih Microsoft Windows**.



**Vsakršnje izboljšave in dodatki so dobrodošli!**