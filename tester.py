import os, re, sys

def main():
	smoVMapi = False

	HW_DIR = os.getcwd()
	if len(sys.argv) == 2:
		imeMapeNaloge = sys.argv[1]
		if os.path.exists(HW_DIR+"\\"+str(imeMapeNaloge)):
			# print "mapa obstaja"
			HW_DIR = HW_DIR+"\\"+str(imeMapeNaloge)
			smoVMapi=False
			
	elif len(sys.argv) == 1:
		smoVMapi=True
	else:
		raise Exception("Podajte samo en argument, ki mora biti ime mape Naloge. Primer:\ntester.exe Naloga1")


	# dobimo trenutni direktorij v katerem smo
	HW_DIR = HW_DIR+"\\log\\"
	MAX_TOCK = 8

	# preverimo ce obstaja mapa
	if os.path.exists(HW_DIR):
		listTestov = os.listdir(HW_DIR)
	else:
		raise Exception("Ne najdem mape "+HW_DIR+"\nTester mora biti v mapi Naloga#")

	# poiscemo max stevilko mape s formatom 'r#'
	maxNum = -1
	for i in listTestov:
		match = re.search(r'r(\d+)', i)
		if match and maxNum < int(match.group(1)):
			maxNum = int(match.group(1))

	if maxNum == -1:
		raise Exception("Ne najdem nobene mape 'r#'")

	CASE_DIR = HW_DIR+"r"+str(maxNum)
	print "Pregledujem direktorij: r"+str(maxNum)+"\nTesti z 0 tockami:"

	vseh = 0
	tocke = 0
	for i in os.listdir(CASE_DIR):
		if "test" in i and "log" in i:	# pol je log od testa
			vseh += 1
			f = open(CASE_DIR+"\\"+i, "r")
			zadnjaVrstica = f.read().splitlines()[-1]
			f.close()
			if "Tocke: 1" in zadnjaVrstica:
				tocke += 1
			elif "Tocke: 0" in zadnjaVrstica:
				print "\t"+i
			else: raise Exception("Ne najdem primerne zadnje vrstice")

	print "Rezultat:", tocke, "tock testnih primerov od max", vseh


	ODDAJA_FILE_DIR = HW_DIR+"oddaja.log"
	if os.path.exists(ODDAJA_FILE_DIR):
		f = open(ODDAJA_FILE_DIR, "r")
		nasel = False
		for i in f.read().splitlines():
			match = re.search(r'r'+str(maxNum)+': tocke=(\d)', i)
			if match:
				nasel = True
				print "TRENUTNO TOCK:", match.group(1), "OD", MAX_TOCK, "(ni nunjo da je tock najvec 8!)"
		f.close()

	if nasel:
		print 'Press enter to exit!'
		raw_input()
	else:
		raise Exception("Ne najdem datoteke oddaja.log")


if __name__ == '__main__':
	try:
		main()

	except SystemExit as e:
		print 'Error!', e
		print 'Press enter to exit!'
		raw_input()
	except Exception as e:
		print 'Error!', e
		print 'Press enter to exit!'
		raw_input()